#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
INTERFACE=tun0

res=`ip addr | grep $INTERFACE | wc -l`

if [ "$res" -eq "0" ]; then
  service openvpn restart
fi


